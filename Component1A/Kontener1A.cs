﻿using System;
using Microsoft;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Component1
{
    public class Kontener1A : AbstractComponent
    {
        public IMagnetron magnetron;

        public Kontener1A()
        {
            this.RegisterRequiredInterface<IMagnetron>();
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (type == typeof(IMagnetron))
            {
                this.magnetron = (IMagnetron)impl;
            }
        }
    }
}
