﻿using ComponentFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Component1;
using Component2;
using MainB;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Kontener1A klasadruga = new Kontener1A();
            klasadruga.RegisterProvidedInterface<IMagnetron>(klasadruga);
            Kontener1A klasapierwsza = new Kontener1A();
            
            kontener.RegisterComponents(klasapierwsza,klasadruga);


            var sprawdzaj = kontener.GetInterface<IMagnetron>();
            klasapierwsza.InjectInterface<IMagnetron>(sprawdzaj);

           // Console.WriteLine(klasapierwsza.Run());
        }
    }
}
