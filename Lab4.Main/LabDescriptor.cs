﻿using System;
using ComponentFramework;
using Lab4.Contract;
using Component1;
using Component2;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Kontener1A);
        public static Type Component2 = typeof(Kontener2A);

        public static Type RequiredInterface = typeof(IMagnetron);

        public static GetInstance GetInstanceOfRequiredInterface = (RequiredInterface) => RequiredInterface;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { (container as Container).RegisterComponent((IComponent)component); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => (container as Container).DependenciesResolved;

        #endregion

        #region P3

        public static Type Component2B = typeof(Component2B.Kontener2B);

        #endregion
    }
}
