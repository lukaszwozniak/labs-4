﻿using Component1;
using Component2B;
using ComponentFramework;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MainB
{
    public class KlasaCzwarta
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Kontener2B klasatrzecia = new Kontener2B();

            klasatrzecia.RegisterProvidedInterface<IMagnetron>(klasatrzecia);

            Kontener1A klasapierwsza = new Kontener1A();
            //klasapierwsza.RegisterProvidedInterface<IMagnetron> ();

            kontener.RegisterComponents(klasapierwsza, klasatrzecia);

        }


    }
}
