﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Component2;
using Contract;

namespace Component1
{
    public class KlasaPierwsza : AbstractComponent
    {
        public IMagnetron magnetron;

        public KlasaPierwsza()
        {

        }
       
        public bool Run()
        {
            return magnetron.Wylacz();
        }
        public bool Go()
        {
            return true;
        }
        public override void InjectInterface(Type type, object impl)
        {
         //   magnetron = (IMagnetron)impl;
        } 
    }
}
